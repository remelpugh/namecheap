﻿namespace Namecheap.DynamicDNS {
    #region USINGS

    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.ServiceProcess;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Xml.Linq;

    #endregion

    /// <summary>
    ///     Namecheap Dynamic DNS updater service.
    /// </summary>
    public partial class Service: ServiceBase {
        #region INSTANCE VARIABLES

        private readonly bool _isEnabled;
        private readonly string _url;
        private bool _isExecuting;
        private Timer _timer;

        #endregion INSTANCE VARIABLES

        #region CONSTRUCTORS

        /// <summary>
        ///     Initializes this instance of <see cref="Service" />.
        /// </summary>
        public Service() {
            InitializeComponent();

            if (!EventLog.SourceExists("Namecheap")) {
                EventLog.CreateEventSource("Namecheap", "Dynamic DNS");
            }

            eventLog.Log = "Dynamic DNS";
            eventLog.Source = "Namecheap";

            _isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["enabled"]);
            _isExecuting = false;
            _url = ConfigurationManager.AppSettings["service_url"];
        }

        #endregion CONSTRUCTORS

        #region PROTECTED PROPERTIES AND METHODS

        #region METHODS

        /// <summary>
        /// </summary>
        protected override void OnContinue() {
            eventLog.WriteEntry("Continue");
            StartTimer();

            base.OnContinue();
        }

        /// <summary>
        ///     Fired on pause of the service.
        /// </summary>
        protected override void OnPause() {
            eventLog.WriteEntry("Paused");
            StopTimer();

            base.OnPause();
        }

        /// <summary>
        ///     Fired on start of the service.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args) {
            eventLog.WriteEntry("Started");
            StartTimer();

            base.OnStart(args);
        }

        /// <summary>
        ///     Fired on stop of the service.
        /// </summary>
        protected override void OnStop() {
            eventLog.WriteEntry("Stopped");
            StopTimer();

            base.OnStop();
        }

        #endregion METHODS

        #endregion PROTECTED PROPERTIES AND METHODS

        #region PRIVATE PROPERTIES AND METHODS

        #region METHODS

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private string GetIpAddress() {
            var ip = string.Empty;

            try {
                using (var client = new WebClient()) {
                    ip = client.DownloadString(ConfigurationManager.AppSettings["ip_url"]);

                    var first = ip.IndexOf("Address: ", StringComparison.Ordinal) + 9;
                    var last = ip.LastIndexOf("</body>", StringComparison.Ordinal);

                    ip = ip.Substring(first, last - first);
                }
            }
            catch (Exception ex) {
                eventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }

            return ip;
        }

        /// <summary>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void HandleTimerElapsed(object sender, ElapsedEventArgs e) {
            var domain = ConfigurationManager.AppSettings["domain"];
            var password = ConfigurationManager.AppSettings["password"];
            var setting = ConfigurationManager.AppSettings["hosts"];

            if (_isExecuting) {
                return;
            }

            _isExecuting = true;

            var ipTask = Task<string>.Factory.StartNew(GetIpAddress);

            await ipTask;

            var ip = ipTask.Result;

            if (string.IsNullOrWhiteSpace(ip)) {
                eventLog.WriteEntry("No IP address found", EventLogEntryType.Warning);
            }
            else {
                eventLog.WriteEntry("IP Address: " + ip);

                var hosts = setting.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x).ToList();

                try {
                    foreach (var host in hosts) {
                        eventLog.WriteEntry("Host: " + host);

                        var url = string.Format(_url, host, domain, password, ip);
                        var updateTask = Task<bool>.Factory.StartNew(() => UpdateHost(url));

                        await updateTask;

                        if (updateTask.Result == false) {
                            eventLog.WriteEntry("Failed to update: " + host, EventLogEntryType.Warning);
                        }
                        else {
                            eventLog.WriteEntry(string.Format("Successfully updated {0}.{1} to {2}", host, domain, ip));
                        }
                    }
                }
                catch (Exception ex) {
                    eventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
                }
            }

            _isExecuting = false;
        }

        /// <summary>
        /// </summary>
        private void StartTimer() {
            _timer = new Timer(double.Parse(ConfigurationManager.AppSettings["interval"]));
            _timer.Elapsed += HandleTimerElapsed;
            _timer.Start();
        }

        /// <summary>
        /// </summary>
        private void StopTimer() {
            if (_timer != null) {
                _timer.Stop();
                _timer.Dispose();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private bool UpdateHost(string url) {
            bool result;

            if (_isEnabled == false) {
                return true;
            }

            try {
                var xml = XDocument.Load(url);
                var done = Convert.ToBoolean(xml.Descendants(XName.Get("Done")).First().Value);
                var errors = Convert.ToInt32(xml.Descendants(XName.Get("ErrCount")).First().Value);

                eventLog.WriteEntry(xml.ToString());

                //<?xml version="1.0"?><interface-response><Command>SETDNSHOST</Command><Language>eng</Language><IP>67.166.182.110</IP><ErrCount>0</ErrCount><ResponseCount>0</ResponseCount><Done>true</Done><debug><![CDATA[]]></debug></interface-response>
                result = (errors == 0 && done);
            }
            catch (Exception ex) {
                result = false;

                eventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }

            return result;
        }

        #endregion METHODS

        #endregion PRIVATE PROPERTIES AND METHODS
    }
}