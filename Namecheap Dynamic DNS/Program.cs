﻿namespace Namecheap.DynamicDNS {
    #region USINGS

    using System;
    using System.IO;
    using System.Reflection;
    using System.ServiceProcess;
    using System.Text;

    #endregion

    internal static class Program {
        private const BindingFlags Flags = BindingFlags.Instance | BindingFlags.NonPublic;

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args) {
            // services that will be started
            var services = new ServiceBase[]{
                new Service()
            };

            // determine which arguments were passed in
            if (args != null && args.Length == 1 && args[0].Length > 1 && (args[0][0] == '-' || args[0][0] == '/')) {
                switch (args[0].Substring(1).ToLower()) {
                    default: {
                        var file = new FileInfo(Assembly.GetExecutingAssembly().Location);
                        var message = new StringBuilder();

                        message.Append("\nUsage: " + file.Name.Replace(file.Extension, ""));
                        message.Append(" [-c] [-i] [-u]\n\n");
                        message.Append("Options: \n");
                        message.Append(
                                       "    -c Runs program in console mode, useful for debugging (application must be compiled as a console application).\n");
                        message.Append("    -i Installs service.\n");
                        message.Append("    -u Uninstalls service.\n");

                        Console.WriteLine(message);
                        break;
                    }
                    case "c":
                    case "console": {
                        var type = typeof (ServiceBase);
                        var method = type.GetMethod("OnStart", Flags);

                        // start all services
                        foreach (var service in services) {
                            method.Invoke(service, new object[]{
                                args
                            });
                        }

                        // pause execution to allow the services to continue running
                        Console.WriteLine("Press any enter to exit");
                        Console.Read();

                        // stop all services
                        foreach (var service in services) {
                            try {
                                service.Stop();
                            }
                            catch {
                            }
                        }
                        break;
                    }
                    case "i":
                    case "install": {
                        SelfInstaller.Install();
                        break;
                    }
                    case "u":
                    case "uninstall": {
                        SelfInstaller.Uninstall();
                        break;
                    }
                }
            }
            else {
                ServiceBase.Run(services);
            }
        }
    }
}