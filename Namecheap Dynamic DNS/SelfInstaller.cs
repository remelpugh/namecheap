﻿using System;
using System.Configuration.Install;
using System.Reflection;

namespace Namecheap.DynamicDNS
{
    /// <summary>
    /// Provides the functionality necessary for a windows service to self install/uninstall.
    /// </summary>
    public class SelfInstaller
    {
        #region INSTANCE VARIABLES

        private static readonly string _path = Assembly.GetExecutingAssembly().Location;

        #endregion INSTANCE VARIABLES

        #region PUBLIC PROPERTIES AND METHODS

        #region METHODS

        /// <summary>
        /// Installs the windows service install the service database.
        /// </summary>
        /// <returns>A <see cref="System.Boolean"/> indicating success.</returns>
        public static bool Install()
        {
            bool returnVal = true;

            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { SelfInstaller._path });
            }
            catch
            {
                returnVal = false;
            }

            return returnVal;
        }

        /// <summary>
        /// Removes the windows service from the service database.
        /// </summary>
        /// <returns>A <see cref="System.Boolean"/> indicating success.</returns>
        public static bool Uninstall()
        {
            bool returnVal = true;

            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { "/u", SelfInstaller._path });
            }
            catch
            {
                returnVal = false;
            }

            return returnVal;
        }

        #endregion METHODS

        #endregion PUBLIC PROPERTIES AND METHODS
    }
}